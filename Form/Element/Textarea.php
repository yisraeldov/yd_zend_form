<?PHP

/**
 * clas for textarea elements
 */
class Yd_Form_Element_Textarea extends Yd_Form_Element{

    
    protected $viewScriptName ='textarea';
    
    public $type = 'textarea';

    public $rows = 5;
    public $cols = 60;

    public $value = null;

}