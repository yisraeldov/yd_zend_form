<?PHP

/**
 * Class for handling form elements
 */
Class Yd_Form_Element{
    

   
    /**
     * path in your view path where form templates are created
     */
    private $viewScriptPath = "forms/elements/";


    /**
     * name of the viewscript to use
     */
    protected $viewScriptName = "element";


    public $label="";
    public $name="";
    public $type="";
    public $value="";
    public $default="";
    public $id=null;

    /**
     * array of Zend_Validate objects to apply when validating elements
     * @author yisraeldov
     */
    public $validators = array();
    public $invalid = false;
    public $messages = array();
    public $errors = array();

    //should we mark this as required using html5, not necessarly validate
    public $required = false;

    /**
     * array of additionaly html attributes
     */
    public $atributes = array();

    public function __construct($name,$options=array()){
        $this->name= $name;

        //this could be safer
        foreach($options as $key=>$option){
            if(property_exists($this,$key)){
                $this->$key = $option;
            }
        }
        if(empty($this->label)){
            $this->label = $this->humanize($this->name);
        }
        
        
        //expand validators
        foreach($this->validators as $key=>$validator){
            if(!is_a($validator,'Zend_Validate_Abstract')){
                if(is_int($key)){
                    $this->validators[$key] = new $validator();
                }
                else{
                    //$key should be the name of the validator class
                    $this->validators[] = new $key($validator);
                    unset($this->validators[$key]);
                }
            }
            if($key=='Zend_Validate_NotEmpty' or is_a($this->validators[$key],'Zend_Validate_NotEmpty')){
                $this->required = true;
            }
        }
    }


    /**
     * make strings more human friendly
     *
     * @param string $string
     * @retrun string
     * @author yisraeldov
     */
    function humanize($string){
        $string = str_replace('_',' ',$string);
        return ucwords(strtolower($string));
    }
    
    /**
     * render the form element
     *
     * @TODO factor this out
     * return string
     */
    public function __toString()
    {
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');

        //pass some veariables to the view        
        $viewRenderer->view->element=$this;

        //todo: try to auto set the name of the viewscript by type
        return $viewRenderer->view->render(
            $this->viewScriptPath.
            $this->viewScriptName.
            ".".$viewRenderer->getViewSuffix()
            );
        
    }
    

    
    /**
     * validate the element
     *
     * This will also set the errors and messages of the element as well as invalid.
     *
     * @param mixed $value
     * @retrun bool
     * @author yisraeldov
     */
    public function validate($value){
        $this->setValue($value);
        $totalValid = true;
        //clear out old errors and messages;
        $this->messages = array();
        $this->errors = array();
        foreach($this->validators as $validator){
            $valid = $validator->isValid($value);
            $totalValid = $totalValid & $valid;
            if(!$valid){
                foreach ($validator->getMessages() as $message) {
                    $this->messages[] = $message;
                }
                foreach ($validator->getErrors() as $error){
                    $this->errors[] = $error;
                }
            }

        }
        $this->invalid = !$totalValid; 
        return $totalValid;
   }
    
    
    /**
     * set the value of the element
     *
     * @param string $data
     * @retrun null
     * @author yisraeldov
     */
    public function setValue($data){
        $this->value = $data;
    }

    public function getValue(){
        return $this->value;
    }
}