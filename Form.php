<?PHP
/**
 * Object for creating and processing forms
 *
 * @author yisraeldov
 */ 

class Yd_Form{

    /**
     * string
     *
     * the action for the form
     */
    public $action = "";

    public $method = "post"; 
    
    public $name = NULL;

    public $invalid =false;
    /**
     * path in your view path where form templates are created
     */
    public $viewScriptPath = "forms/";

    public $elements = array();

    public function __construct($params=array()){
        if(!empty($params['elements'])){
            $this->setElements($params['elements']);
        }

    }

    
    /**
     * Sets the elements, removing any exsisting elements
     *
     * @param array $elements
     * @see Yd_Form::addElements
     * @retrun null
     * @author yisraeldov
     */
    public function setElements($elements=array())
    {
        $this->elements=array();
        $this->addElements($elements);
    }

    /**
     * adds an array of elements
     *
     * @param array $elements the array of elements,
     *  'name'=>'options', or just an array of names, and Yd_Form_Element
     *  objects
     *
     * @retrun null
     * @author yisraeldov
     */
    public function addElements($elements){
        foreach($elements as $element => $options){
            if(is_string($element)){
                $this->addElement($element,$options);
            }
            elseif(is_string($options) or is_a($options,'Yd_Form_Element')){ 
                $this->addElement($options);
            }
        }
    }

    /**
     * adds an element to the form
     *
     * @param string|Yd_Form_Element &$element the element to add or
     * name of element to be created
     *
     * @param array|null the options to use to create a new element,
     * if a Yd_Form_Element is passed for the first paramater this one
     * should be left off
     *
     * @return Yd_Form_Element the element that was added
     * @author yisraeldov
     */
    public function &addElement(&$element,$options=array()){
        if(is_a($element,'Yd_Form_Element')){
            $this->elements[] = $element;
        }
        else{
            if(!is_string($element)){
                throw new Exception("add element expects a string or a Yd_Form_Element");
            }
            $element = new Yd_Form_Element($element,$options);
            $this->elements[] = $element;
        }
        return $element;
    }

    public function __toString()
    {
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');

        //pass some veariables to the view        
        $viewRenderer->view->form=$this;

        return $viewRenderer->view->render($this->viewScriptPath."form.".$viewRenderer->getViewSuffix());
        
    }

    
    /**
     * validate the form
     *
     * @param array $data - the data to pass to the form, the value of all elements will be set
     * @retrun bool
     * @author yisraeldov
     */
    public function validate($data){
        $totalValid = true;
        foreach($this->elements as &$element){
            if(isset($data[$element->name])){
                $totalValid = $totalValid & $element->validate($data[$element->name]);
            }
        }
        $this->invalid = !$totalValid;
        return $totalValid;
    }
    
    public function getData(){
        $data=array();
        foreach($this->elements as $element){
            if(isset($element->value) and $element->getValue() !== NULL){
                $data[$element->name] = $element->getValue();
            }
        }
        return $data;
    }

    
    /**
     * set the form data
     *
     * @param array $data
     * @retrun $data
     * @author yisraeldov
     */
    public function setData($data){
        foreach($this->elements as &$element){
            if( array_key_exists($element->name,$data)){
                $element->setValue($data[$element->name]);
            }
        }
        return $this->getData();

    }
}